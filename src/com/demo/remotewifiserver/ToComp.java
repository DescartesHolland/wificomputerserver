
package com.demo.remotewifiserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;


public class ToComp extends Thread {

	boolean running = false;
	private int SERVERPORT = 5000;
	private ServerSocket serverSocket;
	private Socket client = null;
	private PrintWriter mOut;
/////
	private static InputStreamReader printreader;

	public static void main(String[] args) {

		File file = new File("input.txt"); //create file instance, file to transfer or any data

		ToComp m  = new ToComp();
		m.start();

	}

	@Override
	public void run() {
		super.run();
		running = true;
		
		try {
			System.out.println("Connecting");
			serverSocket = new ServerSocket(SERVERPORT);
			try {
				client = serverSocket.accept();
				System.out.println("Receiving");
//				mOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(client.getOutputStream())));
//				DataOutputStream bw = new DataOutputStream(client.getOutputStream());
				System.out.println("Connecting complete");
				
				DataInputStream in = new DataInputStream(client.getInputStream());
				
				// Here you can connect with database or else you can do what you want with static message
//				mOut.println("HELLO WORLD!");
				String temp = in.readUTF();
				System.out.println(temp);
				
				while(running) {
					
				}
			}
			catch(Exception e) {
				System.out.println("Error:"+e.getMessage());
			} finally {
				client.close();
				System.out.println("Done");
			}
		} catch(Exception e2) {
			System.out.println("Error:"+e2.getMessage());
		}
	}
}
